require 'test_helper'

class PersonInfoControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get person_info_index_url
    assert_response :success
  end

  test "should get create" do
    get person_info_create_url
    assert_response :success
  end

  test "should get delete" do
    get person_info_delete_url
    assert_response :success
  end

end
