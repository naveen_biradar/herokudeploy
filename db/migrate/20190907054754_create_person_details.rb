class CreatePersonDetails < ActiveRecord::Migration[5.2]
  def change
    create_table :person_details do |t|
      t.string :name
      t.integer :age
      t.string :address
      t.integer :contact

      t.timestamps
    end
  end
end
