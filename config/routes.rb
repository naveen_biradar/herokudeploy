Rails.application.routes.draw do
  get 'person_info/index'
  post 'person_info/create'
  get 'person_info/delete'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
